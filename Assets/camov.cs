﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camov : MonoBehaviour
{
    // Start is called before the first frame update
    float inputX, inputZ;

    // Update is called once per frame
    void Update()
    {
        inputZ = Input.GetAxis("Vertical");
        if (inputZ!=0)
            move();
    }
    void move()
    {
        transform.position += transform.forward * inputZ * Time.deltaTime;
    }


}
