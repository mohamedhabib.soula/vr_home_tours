﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LineController : MonoBehaviour
{
    public List<Transform> Positions;
    public float GuideSpeed;

    private LineRenderer lineRenderer;
    

    private int direction = 1;

    public void draw()
    {
        lineRenderer = GetComponent<LineRenderer>();


        lineRenderer.positionCount = Positions.Count;
        lineRenderer.SetPositions(Positions.Select(x => x.transform.position).ToArray());
        

        
    }

    
}